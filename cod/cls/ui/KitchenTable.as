package cls.ui 
{
	import cls.data.PlayerState;
	import cls.data.Repository;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class KitchenTable extends Sprite
	{
		private var _table:table_mc;	
		private var _dragableFood:MovieClip;
		private var _dragablePosX:Array = new Array();
		private var _dragablePosY:Array = new Array();
		private var _changePlayerStatus:Function;
		public function KitchenTable(changePlayerStatus:Function) 
		{
			_changePlayerStatus = changePlayerStatus;
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddTable();
		}
		
		private function AddTable():void 
		{
			_table = new table_mc();
			addChild(_table);
			_table.x =  PlayerState._initPosX;
			_table.y =  PlayerState._initPosY + 40;
			TweenMax.from(_table, .5, {y:PlayerState._initPosY + 500, ease:Back.easeOut});
			SetUpDragableFood()
		}
		
		private function SetUpDragableFood():void 
		{
			_dragablePosX.push();
			_dragablePosY.push();
			for (var i:int = 1; i <= 3; i++)
			{
				_table["food" + i].dragable_food.addEventListener(MouseEvent.MOUSE_DOWN, StartMoveFood);
				_table["food" + i].dragable_food.addEventListener(MouseEvent.MOUSE_UP, StopMoveFood);
				_table["food" + i].dragable_food.addEventListener(MouseEvent.RELEASE_OUTSIDE, StopMoveFood);
				_table["food" + i].percent.text = String(i * 5);
				_dragablePosX.push(_table["food" + i].dragable_food.x);
				_dragablePosY.push(_table["food" + i].dragable_food.y);
			}
		}		
		public function startDragFoodNow():void 
		{
			_dragableFood.x = _table.mouseX - _dragableFood.parent.x;
			_dragableFood.y = _table.mouseY - _dragableFood.parent.y;
			if (_dragableFood.hitTestObject(_table.feedArea))
				PlayerState._feed = true;
			else
				PlayerState._feed = false;
		}
		private function StartMoveFood(e:MouseEvent):void 
		{
			_dragableFood = (e.currentTarget as MovieClip)
			Repository.DragableFood = true;
		}
		
		private function StopMoveFood(e:MouseEvent):void 
		{
			Repository.DragableFood = false;
			var posX = _dragablePosX[int(e.currentTarget.name.slice(4, e.currentTarget.name.length - 1 ))];
			var posY = _dragablePosY[int(e.currentTarget.name.slice(4, e.currentTarget.name.length - 1 ))];	
			
			if (PlayerState._feed == true)
			{
				_dragableFood.alpha = 0;
				_dragableFood.x = posX;
				_dragableFood.y = posY;
				TweenMax.to(_dragableFood, 0.5, {delay:1, alpha:1});
				PlayerState._feed = false;
				_changePlayerStatus(0,int(e.currentTarget.parent.percent.text),0,0);
			}	
			else if (PlayerState._feed == false)
			{
				TweenMax.to(_dragableFood, .5, {x:posX, y:posY});
			}
		}
		
	}

}