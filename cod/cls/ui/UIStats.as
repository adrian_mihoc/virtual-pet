package cls.ui 
{
	import cls.data.PlayerState;
	import cls.data.Repository;
	import cls.data.Room;
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class UIStats extends Sprite
	{
		private var _uiStats:uiStats_mc;
		private var _destroyParent:Function;
		private var _changeRoom:Function;
		
		public function UIStats(destroyParent:Function, changeRoom : Function) 
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
			_destroyParent = destroyParent;
			_changeRoom = changeRoom;
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddUIStats();
		}
		
		private function AddUIStats():void 
		{
			_uiStats = new uiStats_mc();
			addChild(_uiStats);		
			SetupUiButtons();
		}		
		
		private function SetupUiButtons():void 
		{
			for (var i:int = 1; i <= 4; i++)
			{
				_uiStats["btn_" + i].gotoAndStop(i);
				_uiStats["btn_" + i].title_text.text = Room.RoomNames[i - 1];
				_uiStats["btn_" + i].addEventListener(MouseEvent.CLICK, ChangeRoomAtDemand);
			}
			SetUpPlayerStatus();
		}
		public function ChangePlayerStatus(value1:Number=0, value2:Number=0, value3:Number=0, value4:Number=0):void 
		{
			var _lastStatusPercentage:Array;
			_lastStatusPercentage = Main._cookieManager.getStatusPercentage();
			
			trace("000, ", value1, value2, value3, value4);
			
			_lastStatusPercentage[0] = (_lastStatusPercentage[0] + value1);
			_lastStatusPercentage[1] = (_lastStatusPercentage[1] + value2);
			_lastStatusPercentage[2] = (_lastStatusPercentage[2] + value3);
			_lastStatusPercentage[3] = (_lastStatusPercentage[3] + value4);
			
			for (var i:int = 1; i <= 4; i++)
			{
				if (_lastStatusPercentage[i-1] > 100)
				{
					_uiStats["btn_"+i].procent_text.text = "100 %" ;
					_lastStatusPercentage[i-1] = 100;
				}
				else
				{
					_uiStats["btn_"+i].procent_text.text = String(int(_lastStatusPercentage[i-1]) + " %");
				}
			}
			
			
			Main._cookieManager.setOldDate(new Date());
			Main._cookieManager.setStatusPercentage(_lastStatusPercentage);
		}
		private function SetUpPlayerStatus():void 
		{
			var _elapsedTime:int = Repository.getDaysBetweenDates(Main._cookieManager.getOldDate(), new Date());
			var _lastStatusPercentage:Array;
			_lastStatusPercentage = Main._cookieManager.getStatusPercentage();
			
			for (var i:int = 1; i <= 4; i++)
			{
				var statusTimeInterval:int = PlayerState._statusTimeIntervals[i - 1];
				var statusPercentage:int = _lastStatusPercentage[i - 1];
				
				if (int(statusPercentage - (100 * _elapsedTime / statusTimeInterval)) < 0)
				{
					_uiStats["btn_" + i].procent_text.text = "0 %";
					_lastStatusPercentage[i - 1] = 0;
				}
				else
				{
					_uiStats["btn_" + i].procent_text.text = String(int(statusPercentage - (100 * _elapsedTime / statusTimeInterval)) + " %");
					_lastStatusPercentage[i - 1] = int(statusPercentage - (100 * _elapsedTime / statusTimeInterval));
				}
			}
			
			Main._cookieManager.setOldDate(new Date());
			Main._cookieManager.setStatusPercentage(_lastStatusPercentage);
		}
		
		private function ChangeRoomAtDemand(e:MouseEvent):void 
		{
			for (var i:int = 1; i <= 4; i++)
			{
				_uiStats["btn_" + i].removeEventListener(MouseEvent.CLICK, ChangeRoomAtDemand);
			}
			TweenMax.to(this, .3, {alpha:1, onComplete:ReactivateStatusButtons});
			var roomNumber:int = e.currentTarget.name.slice(4, e.currentTarget.name.length);
			//trace("roomNumber = ", roomNumber);
			switch (roomNumber)
			{
				case 1:
					_changeRoom(Room.BATH_ROOM);
				break;
				case 2:
					_changeRoom(Room.KITCHEN_ROOM);
				break;
				case 3:
					_changeRoom(Room.LIVING_ROOM);
				break;
				case 4:
					_changeRoom(Room.BED_ROOM);
				break;
			}
		}
		
		private function ReactivateStatusButtons():void 
		{
			for (var i:int = 1; i <= 4; i++)
			{
				_uiStats["btn_" + i].addEventListener(MouseEvent.CLICK, ChangeRoomAtDemand);
			}
		}
		
	}

}