package cls 
{
	/**
	 * ...
	 * @author TheChosenOne
	 */
	import cls.screens.IntroScreen;
	import cls.screens.GameScreen;
	import cls.screens.rooms.RoomManager;
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class ScreenManager extends MovieClip
	{
		private var _introScreen:IntroScreen;
		private var _gameScreen:GameScreen;
		private var _roomManager:RoomManager;
		
		public function ScreenManager() 
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddIntro();
		}
		
		private function AddIntro():void 
		{
			_introScreen = new IntroScreen(RemoveIntro, AddGame);
			addChild(_introScreen);
		}
		
		private function RemoveIntro():void 
		{
			removeChild(_introScreen);
			_introScreen = null;
		}
		
		private function AddGame()
		{
			_gameScreen = new GameScreen(RemoveGame);
			addChild(_gameScreen);			
		}
		
		private function RemoveGame():void 
		{
			removeChild(_gameScreen);
			_gameScreen = null;
		}
		
		private function Destroy(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, Destroy);
			
		}
		
		
	}

}