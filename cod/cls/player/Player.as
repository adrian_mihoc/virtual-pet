package cls.player
{
	import cls.data.PlayerState;
	import cls.data.Repository;
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class Player extends Sprite
	{
		private var _player:player_mc;		
		private var _left_eyeOX:int;
		private var _left_eyeOY:int;
		private var _right_eyeOX:int;
		private var _right_eyeOY:int;
		private var _left_eyeCenter:Point;
		private var _right_eyeCenter:Point;
		private var _focalPoint:Point;
		
		public function Player()
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddPlayer();
		}
		
		private function AddPlayer():void
		{
			_player = new player_mc();
			addChild(_player);
			ResetPlayersState();
			_player.idle.visible = true;
			SetupEyesMovement();
			Repository.log("Player", "am adaugat pe ecran _player", "AddPlayer Function");
		}
		
		private function SetupEyesMovement():void
		{
			_left_eyeOX = _player.idle.faces.left_eye.eye.x;
			_left_eyeOY = _player.idle.faces.left_eye.eye.y;
			_right_eyeOX = _player.idle.faces.right_eye.eye.x;
			_right_eyeOY = _player.idle.faces.right_eye.eye.y;
			
			_left_eyeCenter = new Point();
			_left_eyeCenter.x = _player.idle.faces.left_eye.center.x;
			_left_eyeCenter.y = _player.idle.faces.left_eye.center.y;
			
			_right_eyeCenter = new Point();
			_right_eyeCenter.x = _player.idle.faces.right_eye.center.x;
			_right_eyeCenter.y = _player.idle.faces.right_eye.center.y;
			
			_focalPoint = new Point();
			_focalPoint.x = (_left_eyeCenter.x + _player.idle.faces.x + _right_eyeCenter.x + _player.idle.faces.x) / 2;
			_focalPoint.y = (_left_eyeCenter.y + _player.idle.faces.y + _right_eyeCenter.y + _player.idle.faces.y) / 2;
		}
		
		public function CheckIfPlayerIdle():Boolean
		{
			if (_player.idle.visible == true)
			{
				return true;
			}
			return false;
		}
		
		public function IdlePlayer():void
		{
			Repository.log("Player", "trebuie sa adaugam animatie de idle", "IdlePlayer Function");
			ResetPlayersState()
			_player.idle.gotoAndPlay(1);
			_player.idle.visible = true;
		}
		
		public function GrabPlayer():void
		{
			Repository.log("Player", "player-ul atarna", "GrabPlayer Function");
			ResetPlayersState()
			_player.grab.gotoAndPlay(1);
			_player.grab.visible = true;
		}
		
		public function RubPlayer():void
		{
			Repository.log("Player", "player-ul este mangaiat", "RubPlayer Function");
		}
		
		public function DropPlayer():void
		{
			ResetPlayersState();
			_player.drop.gotoAndPlay(1);
			_player.drop.visible = true;
			Repository.log("Player", "player-ul cade", "DropPlayer Function");
		}
		
		public function MovePlayer():void
		{
			ResetPlayersState();
			_player.move.gotoAndPlay(1);
			_player.move.visible = true;
			Repository.log("Player", "player-ul se misca", "DropPlayer Function");
		}
		
		public function HitPlayer():void
		{
			ResetPlayersState();
			_player.kick.visible = true;			
			_player.kick.gotoAndPlay("kick"+ int(Math.random()*2+1));
			_player.kick.faces.left_eye.gotoAndPlay("angry");
			_player.kick.faces.right_eye.gotoAndPlay("angry");
			TweenMax.to(_player, .4, {alpha:1,onComplete:ResetKickPlayer});
			Repository.log("Player", "player-ul este lovit", "HitPlayer Function");
		}
		
		private function ResetKickPlayer():void 
		{
			ResetPlayersState();
			_player.idle.visible = true;
			_player.idle.gotoAndPlay(1);
			_player.idle.faces.left_eye.gotoAndPlay("angry");
			_player.idle.faces.right_eye.gotoAndPlay("angry");
			PlayerState._kick = false;
		}
		
		public function MoveEyes():void
		{
			var distx:Number = mouseX - _focalPoint.x;
			var disty:Number = mouseY - _focalPoint.y;
			
			_player.idle.faces.left_eye.eye.x = _player.idle.faces.left_eye.center.x + (distx / 40);
			_player.idle.faces.left_eye.eye.y = _player.idle.faces.left_eye.center.y + (disty / 20);
			_player.idle.faces.right_eye.eye.x = _player.idle.faces.right_eye.center.x - (distx / 40);
			_player.idle.faces.right_eye.eye.y = _player.idle.faces.right_eye.center.y + (disty / 20);
		}
		public function ResetEyes():void
		{
			TweenMax.to(_player.idle.faces.left_eye.eye, 0.2, {x: _left_eyeOX, y: _left_eyeOY});
			TweenMax.to(_player.idle.faces.right_eye.eye, 0.2, {x: _right_eyeOX, y: _right_eyeOY});
		}
		
		private function ResetPlayersState():void
		{
			TweenMax.killTweensOf(_player);
			_player.idle.gotoAndStop(1);
			_player.idle.visible = false;
			
			_player.drop.gotoAndStop(1);
			_player.drop.visible = false;
			
			_player.grab.gotoAndStop(1);
			_player.grab.visible = false;
			
			_player.move.gotoAndStop(1);
			_player.move.visible = false;
			
			_player.kick.gotoAndStop(1);
			_player.kick.visible = false;
		}
	}

}