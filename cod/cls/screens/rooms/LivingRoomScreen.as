package cls.screens.rooms
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class LivingRoomScreen extends Sprite
	{
		
		private var _livingroomScreen: livingroomScreen;
		public function LivingRoomScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddOnStage();
		}
		
		private function AddOnStage():void 
		{
			_livingroomScreen = new livingroomScreen();
			addChild(_livingroomScreen);
		}
		
	}

}