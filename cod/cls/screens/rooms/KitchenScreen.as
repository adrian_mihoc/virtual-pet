package cls.screens.rooms 
{
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class KitchenScreen extends Sprite
	{
		public var _removeTable:Function;		
		private var _kitchenScreen: kitchenScreen;
		private var _addTable:Function;
		
		public function KitchenScreen(addTable:Function, removeTable:Function) 
		{
			_addTable = addTable;
			_removeTable = removeTable;
			addEventListener(Event.ADDED_TO_STAGE, Init);
			addEventListener(Event.REMOVED_FROM_STAGE, Destroy);
			
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddOnStage();
		}
		
		private function AddOnStage():void 
		{
			_kitchenScreen = new kitchenScreen();
			addChild(_kitchenScreen);
			SettupKitchenFurniture();
		}
		
		private function SettupKitchenFurniture():void 
		{
			for (var i:int = 1; i <= 4; i++)
			{
				_kitchenScreen["dulap" + i].gotoAndStop(1);
				_kitchenScreen["sertar" + i].gotoAndStop(1);
				_kitchenScreen["dulap" + i].addEventListener(MouseEvent.CLICK, OpenCloseFurniture);
				_kitchenScreen["sertar" + i].addEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			}
			for (var j:int = 1; j <= 5; j++)
			{
				_kitchenScreen.etajera["sertar" + j].gotoAndStop(1);
				_kitchenScreen.etajera["sertar" + j].addEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			}
			for (var k:int = 1; k <= 2; k++)
			{
				_kitchenScreen.suspendat["usa" + k].gotoAndStop(1);
				_kitchenScreen.suspendat["usa" + k].addEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			}			
			_kitchenScreen.frigider.gotoAndStop(1);
			_kitchenScreen.frigider.addEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			_kitchenScreen.aragaz.gotoAndStop(1);
			_kitchenScreen.aragaz.addEventListener(MouseEvent.CLICK, OpenCloseFurniture);
		}
		
		private function OpenCloseFurniture(e:MouseEvent):void 
		{
			if (e.currentTarget.parent.name == "etajera")
			{
				for (var j:int = 1; j <= 5; j++)
				{
					TweenMax.to(_kitchenScreen.etajera["sertar" + j], 0.2, {frame:1});
				}
			}
			
			if (e.currentTarget.currentFrame < e.currentTarget.totalFrames / 2)
			{
				TweenMax.killTweensOf(e.currentTarget);
				TweenMax.to(e.currentTarget, 0.2, {frame:e.currentTarget.totalFrames});
				if (e.currentTarget.name == "frigider")
				{
					_addTable();
				}
			}
			else if (e.currentTarget.currentFrame >= e.currentTarget.totalFrames / 2)
			{
				TweenMax.killTweensOf(e.currentTarget);
				TweenMax.to(e.currentTarget, 0.2, {frame:1});
				if (e.currentTarget.name == "frigider")
				{
					_removeTable();
				}
			}
		}
		
		
		private function Destroy(e:Event):void 
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, Destroy);
			for (var i:int = 1; i <= 4; i++)
			{
				TweenMax.killTweensOf(_kitchenScreen["dulap" + i]);
				_kitchenScreen["dulap" + i].removeEventListener(MouseEvent.CLICK, OpenCloseFurniture);
				_kitchenScreen["sertar" + i].removeEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			}
			for (var j:int = 1; j <= 5; j++)
			{
				TweenMax.killTweensOf(_kitchenScreen.etajera["sertar" + j]);
				_kitchenScreen.etajera["sertar" + j].removeEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			}
			for (var k:int = 1; k <= 2; k++)
			{
				TweenMax.killTweensOf(_kitchenScreen.suspendat["usa" + k]);
				_kitchenScreen.suspendat["usa" + k].removeEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			}	
			TweenMax.killTweensOf(_kitchenScreen.frigider);
			TweenMax.killTweensOf(_kitchenScreen.aragaz);
			_kitchenScreen.frigider.removeEventListener(MouseEvent.CLICK, OpenCloseFurniture);
			_kitchenScreen.aragaz.removeEventListener(MouseEvent.CLICK, OpenCloseFurniture);
		}
	}

}