package cls.screens.rooms 
{
	import cls.data.Repository;
	import cls.data.Room;
	import com.greensock.TweenMax;
	import flash.display.MovieClip;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class RoomManager extends MovieClip
	{
		private var _bathroomScreen:BathRoomScreen;
		private var _bedroomScreen:BedRoomScreen;
		private var _kitchenScreen:KitchenScreen;
		private var _livingroomScreen:LivingRoomScreen;
		private var _playgroundScreen:PlayGroundScreen;
		private var _addTable:Function;
		private var _removeTable:Function;
		
		public function RoomManager(removeTable:Function, addTable : Function) 
		{
			_addTable = addTable;	
			_removeTable = removeTable;
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);			
		}
		private function AnimateRooms():void
		{
			if (this.numChildren == 1)
			{
				TweenMax.to(this.getChildAt(0), .3, {x:this.getChildAt(0).x - stage.stageWidth});
			}
			else if (this.numChildren > 1)
			{
				for (var i:int = 0; i <= this.numChildren - 1; i++)
				{
					TweenMax.to(this.getChildAt(i), .3, {x:this.getChildAt(i).x - stage.stageWidth});
				}
				TweenMax.to(this.getChildAt(1), .3, {alpha:1, onComplete:RemovePreviousRoom});
			}
			
		}
		
		private function RemovePreviousRoom():void 
		{			
			Repository.removeAllChildren(this.getChildAt(0));
			trace("numarul de camere = ", this.numChildren);
		}
		public function AddRoom(room:String):void 
		{
			switch (room)
			{
				case Room.LIVING_ROOM:
					{
						_removeTable();
						_livingroomScreen = new LivingRoomScreen();
						addChild(_livingroomScreen);
						_livingroomScreen.x = stage.stageWidth;
						Repository.log("RoomManager", "am adaugat pe ecran _livingroomScreen" , "AddRoom Function");
					}
				break;
				case Room.PLAY_ROOM:
					{
						_removeTable();
						_playgroundScreen = new PlayGroundScreen();
						addChild(_playgroundScreen);
						_playgroundScreen.x = stage.stageWidth;
						Repository.log("RoomManager", "am adaugat pe ecran _playgroundScreen" , "AddRoom Function");
					}
				break;
				case Room.BATH_ROOM:
					{
						_removeTable();
						_bathroomScreen = new BathRoomScreen();
						addChild(_bathroomScreen);
						_bathroomScreen.x = stage.stageWidth;
						Repository.log("RoomManager", "am adaugat pe ecran _bathroomScreen" , "AddRoom Function");
					}
				break;
				case Room.KITCHEN_ROOM:
					{
						_removeTable();
						_kitchenScreen = new KitchenScreen(AddKitchenTable, RemoveKitchenTable);
						addChild(_kitchenScreen);
						_kitchenScreen.x = stage.stageWidth;
						Repository.log("RoomManager", "am adaugat pe ecran _kitchenScreen" ,"AddRoom Function");
					}
				break;
				case Room.BED_ROOM:
					{
						_removeTable();
						_bedroomScreen = new BedRoomScreen();
						addChild(_bedroomScreen);
						_bedroomScreen.x = stage.stageWidth;
						Repository.log("RoomManager", "am adaugat pe ecran _bedroomScreen" ,"AddRoom Function");
					}
				break;
			}
			AnimateRooms();
		}
		
		private function RemoveKitchenTable():void 
		{
			_removeTable();
		}
		
		private function AddKitchenTable():void 
		{
			_addTable();
		}
	}
}