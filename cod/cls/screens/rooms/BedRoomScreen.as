package cls.screens.rooms 
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class BedRoomScreen extends Sprite
	{
		
		private var _bedroomScreen: bedroomScreen
		public function BedRoomScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddOnStage();
		}
		
		private function AddOnStage():void 
		{
			_bedroomScreen = new bedroomScreen();
			addChild(_bedroomScreen);
		}
		
	}

}