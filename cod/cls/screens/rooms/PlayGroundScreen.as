package cls.screens.rooms 
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class PlayGroundScreen extends Sprite
	{
		
		private var _playgroundScreen: playgroundScreen;
		public function PlayGroundScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddOnStage();
		}
		
		private function AddOnStage():void 
		{
			_playgroundScreen = new playgroundScreen();
			addChild(_playgroundScreen);
		}
		
	}

}