package cls.screens.rooms 
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class BathRoomScreen extends Sprite
	{
		private var _bathroomScreen: bathroomScreen
		public function BathRoomScreen() 
		{
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddOnStage();
		}
		
		private function AddOnStage():void 
		{
			_bathroomScreen = new bathroomScreen();
			addChild(_bathroomScreen);
		}
		
		
	}

}