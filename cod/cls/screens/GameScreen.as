package cls.screens 
{
	import cls.data.PlayerState;
	import cls.data.Repository;
	import cls.data.Room;
	import cls.player.Player;
	import cls.screens.rooms.RoomManager;
	import cls.ui.UIStats;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import cls.ui.KitchenTable;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class GameScreen extends MovieClip
	{
		private var _destroyParent:Function;
		private var _roomManager:RoomManager;
		private var _player:Player;
		private var _uiStats:UIStats;
		private var _currentTouchY:int;
		private var _startTouchY:int;
		private var _difY:int;
		private var _difX:int;
		private var _distanceOverTime:Number;
		private var _kitchenTable:KitchenTable;
		
		public function GameScreen(destroyParent:Function) 
		{
			_destroyParent = destroyParent;			
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			AddRoom();
			AddUIStats();
			AddPlayer();
			AddPlayerEvents();			
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}	
		
		private function AddPlayer():void 
		{
			_player = new Player();
			PlayerState._initPosX = stage.stageWidth / 2;
			PlayerState._initPosY = stage.stageHeight / 10 * 9;
			_player.x = PlayerState._initPosX;
			_player.y = PlayerState._initPosY;			
			addChild(_player);
		}
		
		
		private function AddUIStats():void 
		{
			_uiStats = new UIStats(RemoveUI, ChangeRoom);
			addChild(_uiStats);
		}
		private function ChangePlayerStatus(health:Number=0, food:Number=0, happy:Number=0, sleep:Number=0):void
		{
			if (_uiStats != null)
			{
				trace(health, food, happy, sleep);
				_uiStats.ChangePlayerStatus(health, food, happy, sleep);
			}
		}
		private function ChangeRoom(room:String):void 
		{
			_roomManager.AddRoom(room);
			_roomManager.x = stage.stageWidth / 2;
			_roomManager.y = stage.stageHeight / 2;
		}
		
		private function RemoveUI():void 
		{
			removeChild(_uiStats);
			_uiStats = null;
		}
		
		private function AddRoom():void 
		{
			_roomManager = new RoomManager(RemoveKitchenTable, AddKitchenTable)
			addChild(_roomManager);
			_roomManager.AddRoom(Room.LIVING_ROOM);
			_roomManager.x = stage.stageWidth / 2;
			_roomManager.y = stage.stageHeight / 2;
		}
		
		private function RemoveKitchenTable():void 
		{
			if (_kitchenTable != null)
			{
				TweenMax.to(_kitchenTable, .3, {y:_kitchenTable.y + 150, onComplete:RemoveKitchenTableFromStage});				
			}
		}
		
		private function RemoveKitchenTableFromStage():void 
		{
			if (_kitchenTable != null)
			{
				TweenMax.killTweensOf(_kitchenTable);
				removeChild(_kitchenTable);
				_kitchenTable = null;
			}
		}
		
		private function AddKitchenTable():void 
		{
			TweenMax.killTweensOf(_player);
			TweenMax.to(_player, .5, {x:PlayerState._initPosX, y:PlayerState._initPosY, ease:Back.easeOut});
			_player.IdlePlayer();
			RemoveKitchenTableFromStage();			
			if (_kitchenTable == null)
			{
				_kitchenTable = new KitchenTable(ChangePlayerStatus);
				addChild(_kitchenTable);
			}
		}
		
		private function AddPlayerEvents():void 
		{
			_player.addEventListener(MouseEvent.MOUSE_DOWN, StarTouchPlayer);
			_player.addEventListener(MouseEvent.MOUSE_UP, StopTouchPlayer);
			_player.addEventListener(MouseEvent.RELEASE_OUTSIDE, StopTouchPlayer);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, StartMovePlayer);
			stage.addEventListener(MouseEvent.MOUSE_UP, StopMovePlayer);			
		}
		
		private function StopMovePlayer(e:MouseEvent):void 
		{
			_player.ResetEyes();
			PlayerState._eyeLook = false;
		}
		
		private function StartMovePlayer(e:MouseEvent):void 
		{
			PlayerState._eyeLook = true;
			if (PlayerState._move == false)
			{
				if (mouseX < _player.x)
					_player.scaleX = -1;
				if (mouseX > _player.x)
					_player.scaleX = 1;
			}
			
			if (_kitchenTable != null)
			return;
			
			if (mouseY < PlayerState._initPosY - 50)
				return;	
				
			if (mouseX > _player.x - 50 && mouseX < _player.x + 50) 
			return;
			
			StartMovement();
		}
		
		private function StartMovement():void 
		{
			if (mouseX < _player.x)
				_player.scaleX = -1;
			if (mouseX > _player.x)
				_player.scaleX = 1;
			PlayerState._move = true;
			TweenMax.killTweensOf(_player);
			_player.MovePlayer();
			_distanceOverTime = Math.abs(_player.x-mouseX) / 65;
			trace(_player.x-mouseX, _distanceOverTime);
			TweenMax.to(_player, _distanceOverTime, {x:mouseX, onComplete:ResetStageClick, ease:Linear.easeNone});
			
		}
		
		private function ResetStageClick():void 
		{
			if (PlayerState._kick == false)
			{
				_player.IdlePlayer();
			}
			//trace('asdasdasdasdasdasdasdasdasdasdasdasdasd');
			//PlayerState._move = false;
		}
		
		private function onEnterFrame(e:Event):void 
		{
			_currentTouchY = mouseY;
// DRAG FOOD CONDITIONS
			if (Repository.DragableFood == true && _kitchenTable != null)
			{
				_kitchenTable.startDragFoodNow();
			}
			
// EYE MOVEMENT CONDITIONS
			if (_player.CheckIfPlayerIdle() == true && PlayerState._eyeLook == true)
			{
				_player.MoveEyes();
			}
// RUBBING PLAYER CONDITIONS
			if (PlayerState._touch == true)
			{
				if (_currentTouchY - _startTouchY > 0 && PlayerState._rub == false && PlayerState._grab == false && PlayerState._move == false)
				{
					// draging goes down					
						_player.RubPlayer();
						PlayerState._rub = true;
						PlayerState._grab = false;
						PlayerState._kick = false;
						PlayerState._move = false;
						
				}
				if (_currentTouchY - _startTouchY < 0 && PlayerState._rub == false && PlayerState._grab == false)
				{
					// draging goes up
					if (_currentTouchY - _startTouchY < -20)
					{
						TweenMax.killTweensOf(_player);
						stage.removeEventListener(MouseEvent.MOUSE_DOWN, StartMovePlayer);
						stage.addEventListener(MouseEvent.MOUSE_DOWN, StartMovePlayer);
						_player.GrabPlayer();
						PlayerState._rub = false;
						PlayerState._grab = true;
						PlayerState._kick = false;
						PlayerState._move = false;
					}
					
				}
				if (PlayerState._grab == true)
				{
					_player.x = mouseX + _difX;
					_player.y = mouseY + _difY;
				}
			}
			
		}
		private function StarTouchPlayer(e:MouseEvent):void 
		{
			trace(" PlayerState._move " , PlayerState._move)
			
			PlayerState._touch = true;
			_startTouchY = mouseY;
			_difX = e.currentTarget.x - mouseX;
			_difY = e.currentTarget.y - mouseY;
			//_player.GrabPlayer();
		}
		private function StopTouchPlayer(e:MouseEvent):void 
		{
			
			if (_currentTouchY - _startTouchY > -20)
			{
				if (PlayerState._move == false)
				{
					_player.HitPlayer();
					PlayerState._kick = true;
				}
			}
			if (PlayerState._grab == true)
			{
				DropPlayer();
			}
			PlayerState._touch = false;	
			PlayerState._grab = false;
			PlayerState._rub = false;
			
		}
		private function DropPlayer():void
		{
			_player.DropPlayer();
			TweenMax.to(_player, .5, {y:PlayerState._initPosY});
			TweenMax.to(_player, .9, {alpha:1,onComplete:ResetPlayerPos});
		}
		
		private function ResetPlayerPos():void 
		{
			_player.IdlePlayer();
		}
		
		
	}

}





/*private function addMovingEyes()
		{
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_DOWN, StartDrag);
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_UP, ResetEyePosition);
			
			_left_eyeOX = pet.pers.pers.eyes.left_eye.x;
			_left_eyeOY = pet.pers.pers.eyes.left_eye.y;
			_right_eyeOX = pet.pers.pers.eyes.right_eye.x;
			_right_eyeOY = pet.pers.pers.eyes.right_eye.y;
			
			_left_eyeCenter = new Point();
			_left_eyeCenter.x = pet.pers.pers.eyes.left.x;
			_left_eyeCenter.y = pet.pers.pers.eyes.left.y;
			
			_right_eyeCenter = new Point();
			_right_eyeCenter.x = pet.pers.pers.eyes.right.x;
			_right_eyeCenter.y = pet.pers.pers.eyes.right.y;
			
			_focalPoint = new Point();
			_focalPoint.x = (_left_eyeCenter.x + pet.pers.pers.x + _right_eyeCenter.x + pet.pers.pers.x) / 2;
			_focalPoint.y = (_left_eyeCenter.y + pet.pers.pers.y + _right_eyeCenter.y + pet.pers.pers.y) / 2;
			
			gameConfig.stageRef.addEventListener(MouseEvent.MOUSE_DOWN, StartDrag);
			gameConfig.stageRef.addEventListener(MouseEvent.MOUSE_UP, ResetEyePosition);
		}
		
		private function StartDrag(e:MouseEvent):void
		{
			gameConfig.stageRef.addEventListener(MouseEvent.MOUSE_MOVE, LookAtMouse);
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_DOWN, StartDrag);
		}
		
		private function LookAtMouse(e:MouseEvent):void
		{
			var distx:Number = mouseX - _focalPoint.x;
			var disty:Number = mouseY - _focalPoint.y;
			
			pet.pers.pers.eyes.left_eye.x = pet.pers.pers.eyes.left.x + (distx / 50);
			pet.pers.pers.eyes.left_eye.y = pet.pers.pers.eyes.left.y + (disty / 50);
			pet.pers.pers.eyes.right_eye.x = pet.pers.pers.eyes.right.x + (distx / 50);
			pet.pers.pers.eyes.right_eye.y = pet.pers.pers.eyes.right.y + (disty / 50);
		}
		
		private function ResetEyePosition(e:MouseEvent):void
		{
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_MOVE, LookAtMouse);
			gameConfig.stageRef.addEventListener(MouseEvent.MOUSE_DOWN, StartDrag);
			TweenLite.to(pet.pers.pers.eyes.left_eye, 0.2, {x: _left_eyeOX, y: _left_eyeOY});
			TweenLite.to(pet.pers.pers.eyes.right_eye, 0.2, {x: _right_eyeOX, y: _right_eyeOY});
		}
		
		private function removeMovingEyes()
		{
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_MOVE, LookAtMouse);
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_DOWN, StartDrag);
			gameConfig.stageRef.removeEventListener(MouseEvent.MOUSE_UP, ResetEyePosition);
		}*/