package cls.screens 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class IntroScreen extends MovieClip
	{
		private var _destroyParent : Function;
		private var _addGame : Function;
		
		public function IntroScreen(destroyParent:Function, addGame : Function) 
		{
			_destroyParent = destroyParent;
			_addGame = addGame;
			
			addEventListener(Event.ADDED_TO_STAGE, Init);
		}
		
		private function Init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, Init);
			
			// HERE WE ADD INTRO STUFF
			
			_addGame();
			_destroyParent();
		}
		
		
	}

}