package cls.data
{
	/**
	 * ...
	 * @author TheChosenOne
	 */
	
	public class Room 
	{
		public static const LIVING_ROOM:String = "living";
		public static const BATH_ROOM:String = "bath";
		public static const KITCHEN_ROOM:String = "kitchen";
		public static const PLAY_ROOM:String = "playground";
		public static const BED_ROOM:String = "bed";
		public static var RoomNames:Array = new Array("health", "food", "happy", "sleep");
			
	}
}