package cls.data 
{
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class Repository 
	{
		
		public static var lol:int;
		public static var DragableFood:Boolean;
		
		public function Repository() 
		{
			
		}		
		static public function log(className:String="", message:String = "", subject:String = ""):void 
		{
			trace(className, ": ", message, " - ", subject);
		}
		public static function getDaysBetweenDates(date1:Date, date2:Date):int
		{			
			var one_minute:Number = 1000*60;
			var one_hour:Number = 1000 * 60 * 60;
			var one_day:Number = 1000 * 60 * 60 * 24;
			var date1_ms:Number = date1.getTime();
			var date2_ms:Number = date2.getTime();
			var difference_ms:Number = Math.abs(date1_ms - date2_ms)
			return Math.round(difference_ms / one_minute);
		}
		
		public static function randomSort(a:*, b:*):Number
		{
			if (Math.random() < 0.5) return -1;
			else return 1;
		}
		
		public static function removeAllChildren(parentChild:*):void
		{
			for (var i:uint = 0; i < parentChild.numChildren; ++i)
			{
				//check if child is a DisplayObjectContainer, which could hold more children
				if (parentChild.getChildAt(i) is DisplayObjectContainer) removeAllChildren(DisplayObjectContainer(parentChild.getChildAt(i)));
				else
				{
					//remove and null child of parent
					var child:DisplayObject = parentChild.getChildAt(i);
					parentChild.removeChild(child);
					child = null;
				}
				
			}
			//remove and null parent
			parentChild.parent.removeChild(parentChild);
			parentChild = null;
		}
	}

}