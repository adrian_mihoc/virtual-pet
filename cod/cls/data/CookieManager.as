package cls.data 
{
    import flash.display.MovieClip;
    import flash.net.SharedObject;
    import flash.utils.getDefinitionByName;
    /**
     * ...
     * @author Alex
     */
    public class CookieManager 
    {
        private var _cookieData : SharedObject;
        private var _oldDate : Date;
        private var _statusPercentage : Array;
        
        public function CookieManager() 
        {
            initCookies();
        }
        
        private function initCookies():void 
        {
            _cookieData = SharedObject.getLocal("virtualPetArpamedia");
            
            //setarea initiala a datelor in cazul in care nu exista
           readCookies();
        }
        
        private function readCookies():void 
        {
            if (_cookieData.data._oldDate == undefined)
            {
				_oldDate = new Date();
                _cookieData.data._oldDate = _oldDate;
            }
			if (_cookieData.data._statusPercentage == undefined)
            {
				_statusPercentage = new Array(15,15,15,15);
                _cookieData.data._statusPercentage = _statusPercentage;
            }
            _cookieData.flush();
        }
        public function getStatusPercentage() : Array
        {
            return  _cookieData.data._statusPercentage;
        }
		public function setStatusPercentage(value:Array)
        {
            _cookieData.data._statusPercentage = value;
            _cookieData.flush();
        }
        public function getOldDate() : Date
        {
            return  _cookieData.data._oldDate;
        }
        public function setOldDate(value : Date)
        {
            _cookieData.data._oldDate = value;
            _cookieData.flush();
        }
    }
}