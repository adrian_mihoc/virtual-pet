package cls.data
{
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class PlayerState 
	{
		public static var _rub:Boolean = false;
		public static var _grab:Boolean = false;
		public static var _kick:Boolean = false;
		public static var _move:Boolean = false;
		public static var _touch:Boolean = false;
		public static var _eyeLook:Boolean = false;
		public static var _feed:Boolean = false;
		public static var _initPosX:int;
		public static var _initPosY:int;
		public static var _statusTimeIntervals:Array = new Array(240,240,240,240); // minute
	}

}