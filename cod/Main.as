package
{
	import cls.ScreenManager;
	import cls.data.Repository;
	import cls.data.CookieManager;
	import com.greensock.plugins.FramePlugin;
	import com.greensock.plugins.TweenPlugin;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author TheChosenOne
	 */
	public class Main extends Sprite 
	{
		private var _screenManager:ScreenManager;
		public static var _cookieManager:CookieManager;
		public function Main() 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			TweenPlugin.activate([FramePlugin]);
			
			InitCookiesBeforeAnything();
			AddScreenManager();
		}
		
		private function InitCookiesBeforeAnything():void 
		{
			_cookieManager = new CookieManager();
		}
		
		private function AddScreenManager():void 
		{
			_screenManager = new ScreenManager();
			addChild(_screenManager);
		}
		
	}
	
}